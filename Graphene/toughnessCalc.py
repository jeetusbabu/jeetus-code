import pandas as pd
import numpy as np
from scipy.integrate import trapz

# Load the ODS file into a pandas DataFrame
data = pd.read_csv('toughness.csv')  # Make sure to provide the correct path

# Assuming your columns are named 'x' and 'y'; adjust these if needed
x = data['x']
y = data['y']

# Calculate the area under the curve using the trapezoidal rule
area = trapz(y, x)

# Print the result
print(f'Area under the curve: {area}')
