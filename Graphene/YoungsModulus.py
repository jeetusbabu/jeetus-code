import pandas as pd
import matplotlib.pyplot as plt

# Function to calculate Young's Modulus
def calculate_youngs_modulus(stress, strain):
    if len(stress) != len(strain):
        raise ValueError("Stress and strain lists must have the same length")
    
    # Calculate the slope of the stress-strain curve
    slope = (stress[-1] - stress[0]) / (strain[-1] - strain[0])
    
    return slope

# Read data from CSV file (replace 'data.csv' with your file path)
data = pd.read_csv('YM.csv')

# Extract stress and strain columns from the CSV file
stress_data = data['y'].tolist()
strain_data = data['x'].tolist()

# Calculate Young's Modulus
youngs_modulus = calculate_youngs_modulus(stress_data, strain_data)

# Plot the stress-strain curve
plt.plot(strain_data, stress_data, marker='o', linestyle='-')
plt.title("Stress-Strain Curve")
plt.xlabel("Strain")
plt.ylabel("Stress")
plt.grid()
plt.show()

# Display the calculated Young's Modulus
print(f"Young's Modulus (E) = {youngs_modulus} GPa")
