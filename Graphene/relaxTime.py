import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

# Load data from the stress autocorrelation function file
data = np.loadtxt('stress_corr1(200).out')

# Extract time and autocorrelation values
time = data[:, 0]
autocorr = data[:, 1]

# Define the exponential decay function
def exponential_decay(t, C0, tau):
    return C0 * np.exp(-t / tau)

# Fit the autocorrelation function to the exponential decay function
popt, pcov = curve_fit(exponential_decay, time, autocorr)

# Extract the relaxation time (tau) from the fit
tau = popt[1]

# Plot the autocorrelation function and the fit
plt.plot(time, autocorr, label='Autocorrelation')
plt.plot(time, exponential_decay(time, *popt), label='Fit')
plt.xlabel('Time')
plt.ylabel('Autocorrelation')
plt.legend()
plt.show()

# Print the relaxation time
print('Relaxation time (tau):', tau)

